import math

import vector_util as vector
import util


def circle(x, y, ox=0, oy=0):
    distance = math.sqrt((x-ox)**2+(y-oy)**2) - 3
    return distance

def world(x, y):
    return(
        min(
            circle(x,y, 10, 0),
            circle(x,y, -10, 0)
            ))

# normalizes the vector between two points into a vector centered on origin
def pnormalize(p1, p2):
    d1 = ((p1[0]+p2[0])/2, (p1[1]+p2[1])/2)
    return vector.normalize(d1)


# cast a ray from given origin throug a remote point, given a distance
def cast(origin, remote, distance):
    # obtain a "lookat" vector from the origin to the point
    ## FIXME move this line back to the pnormalize function ?
    norman = vector.normalize((remote[0] - origin[0], remote[1] - origin[1]))
    # set the distance of the vector to the desired cast distance
    long_norman = vector.scale(norman, distance)
    # translate the vector to the origin of the ray
    door = vector.translate(long_norman, origin)
    return(door)

def render_ray(camera_pos, remote):
    r = {}
    r["steps"] = 0
    r["hit"] = False
    r["total"] = 0.0
    r["path"] = []
    r["remote"] = remote
    r["color"] = (0,0,0)

    nextp = cast(camera_pos, remote, r["total"])
    #min_safe = circle(camera_pos[0], camera_pos[1])
    min_safe = world(camera_pos[0], camera_pos[1])
    while r["steps"] < 8:
        r["path"].append(nextp)
        # reduce the minimum distance to avoid hard edges on curved surfaces
        if min_safe <= 1/64:
            r["hit"] = True
            tile = ((math.sin(nextp[1])+math.sin(nextp[0])+2)/4)
            r["color"] = (tile * 255,tile * 255,tile * 255)
            break
        r["total"] += min_safe
        r["steps"] += 1
        nextp = cast(camera_pos, remote, r["total"])
        #nextp = cast(remote, camera_pos, r["total"])
        #min_safe = circle(nextp[0], nextp[1])
        min_safe = world(nextp[0], nextp[1])


    return r


def render_line_X(camera_pos, screen_width, screen_distance, camera_angle):
    line_data = []
    screen_halfwidth = int(screen_width / 2)
    # specify the FOV for the camera rendering
    fov = 90.0
    # iterate over each screen pixel
    for scanline in range(0, screen_width, 1):
        # determine the angle of this scanline
        w = (scanline / screen_width * fov) + 45 + math.degrees(camera_angle)
        # set the remote as the sum of the camera position and a vector with the scanline angle
        rem = (camera_pos[0]+math.cos(w * math.pi / 180) * screen_distance, camera_pos[1]+math.sin(w * math.pi / 180) * screen_distance)
        #rem = (camera_pos[0]+w/8, camera_pos[1]+screen_distance)
        ray_data = render_ray(camera_pos, rem)
        line_data.append(ray_data)
    return line_data


def render_line(camera_pos, screen_width, screen_distance, camera_angle):
    line_data = []
    fov = math.radians(90)
    #fov = 170.0
    #fov = 100.0

    sx = math.cos(-(fov/2) + camera_angle)
    ex = math.cos(fov/2 + camera_angle)

    sy = math.sin(-(fov/2) + camera_angle)
    ey = math.sin(fov/2 + camera_angle)

    dstart = vector.scale((sx, sy), screen_distance)
    dend = vector.scale((ex, ey), screen_distance)

    start = vector.translate(dstart, camera_pos)
    end = vector.translate(dend, camera_pos)

    for w in range(0, screen_width, 1):
        rem = util.lerp2(start, end, (w / screen_width))
        lookat_rem = (rem[0] - camera_pos[0], rem[1] - camera_pos[1])
        data = render_ray(camera_pos, rem)
        data["perp"] = math.sin(vector.get_angle(lookat_rem)+camera_angle) * data["total"]
        line_data.append(data)

    return line_data



#[desired_scanline / screen_width] * [(end-start) / mag(end-start))]
#The vector with angle (90 - (fov /2)), where the origin is the camera position
#The vector with angle (90 - (fov /2)) + fov, where the origin is the camera position
#[(cos a, sin a) * length] + origin
#rem = (w / screen_width) * (vector.divide_value(vector.subtract(end, start), vector.magnitude(vector.subtract(end, start))))
#rem = vector.scale(vector.divide_value(vector.subtract(end, start), vector.magnitude(vector.subtract(end, start))), (w / screen_width))



#def render_line(camera_pos, screen_width, screen_distance):
#    distances = []
#    #camera_pos = (0, 0)
#    #screen_distance = 10
#    #screen_width = 20
#    #screen_width = 100
#    screen_halfwidth = int(screen_width / 2)
#    for w in range(-screen_halfwidth, screen_halfwidth+1, 1):
#        #rem = (w, screen_distance)
#        #rem = (camera_pos[0]+w, camera_pos[1]+screen_distance)
#        #rem = (camera_pos[0]+w, camera_pos[1]+screen_distance)
#        rem = (camera_pos[0]+w, camera_pos[1]+screen_distance)
#        #cake = cast(camera_pos, rem, 0)
#        min_safe = circle(camera_pos[0], camera_pos[1])
#        t = {}
#        t["total"] = min_safe
#        t["steps"] = 0
#        while t["steps"] < 20 and min_safe > 0:
#            cake = cast(camera_pos, rem, t["total"])
#            min_safe = circle(cake[0], cake[1])
#            t["total"] += min_safe
#            t["steps"] += 1
#
#        #distances.append(int(t["total"]))
#        distances.append(t["total"])
#
#    return(distances)
